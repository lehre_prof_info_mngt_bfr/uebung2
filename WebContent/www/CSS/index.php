<?php 
require_once '../smarty.php';
$tpl = new SMTemplate();
$tpl->render('index', array(
	'template' => 'hello',
	'newsfeed' => array(
		array(
			'time' => '23-04-16',
			'message' => 'Bauernhof Krause baut nun auch Tomaten an.'
		),
		array(
			'time' => '15-04-16',
			'message' => 'Erdbeeren sind dieses Jahr besonders saftig.'
		),
		array(
			'time'=> '28-04-16',
			'message' => 'Erstbesteller erhalten eine Gurke <span class="red">gratis</span>!'
		)	
		
	)
		
));

?>