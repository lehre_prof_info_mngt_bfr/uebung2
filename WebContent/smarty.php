<?php 
/**
 * @file
 * Wrapper for Smarty Template Engine
 */
require_once 'smarty/libs/Smarty.class.php';
$smtemplate_config = array(
		'template_dir' => __DIR__ . '/templates',
		'compile_dir' => __DIR__ . '/templates_c',
		'cache_dir' => __DIR__ . '/cache'
);

class SMTemplate{
	 
	private $_smarty;
	 
	function __construct(){
		$this->_smarty = new Smarty();
		 
		global $smtemplate_config;
		$this->_smarty->template_dir = $smtemplate_config['template_dir'];
		$this->_smarty->compile_dir = $smtemplate_config['compile_dir'];
		$this->_smarty->cache_dir = $smtemplate_config['cache_dir'];
	}
	
	function render($template, $data = array()){
	    foreach($data as $key => $value){
	        $this->_smarty->assign($key, $value);
	    }
	    $this->_smarty->display($template . '.tpl');
	}
}
?>