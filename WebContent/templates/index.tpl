<!DOCTYPE html>
<html>
<head>
    <title>Leipziger Biokiste</title>
    <link rel="stylesheet" type="text/css" href="CSS/stylesheet.css">
</head>
<body>
    <nav>
        <a href="feedback.html">Feedback</a>
        <a href="kontakt.html">Kontakt</a>
    </nav>
    <!-- <p>{include file="$template.tpl"}</p> -->
    <div id="news">
        <ul> 
        	{foreach $newsfeed as $news}
        	  <li>
        		<time datetime="{$news.time}">{$news.time}</time>
                <div>
                    {$news.message}
                </div>
              </li>
        	{/foreach}
        </ul>
    </div>
    <div id="content">
        <h1>Leipziger Biokiste</h1>
        <div>Die Experten f�r Bio-Gem�se!</div>
        <div>
            <ol>
                <li>
                    <div>Biokiste mini</div>
                    <div>10€/Monat</div>
                </li>
                <li>
                    <div>Biokiste standard</div>
                    <div>20€/Monat</div>
                </li>
                <li>
                    <div>Biokiste maxi</div>
                    <div>30€/Monat</div>
                </li>
            </ol>
        </div>
    </div>
</body>
</html>
